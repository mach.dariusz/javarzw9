package com.sda.spring;

import com.sda.spring.game.model.Game;
import com.sda.spring.game.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Arrays;

@SpringBootApplication
public class Application {

    @Autowired
    private ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    public void init() {

        System.out.printf("Number of beans: %s\n", context.getBeanDefinitionCount());

        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.out::println);

        final Game game1 = context.getBean("game", Game.class);
        System.out.println("\nGame details below:\n");
		System.out.println("\t" + game1);
		System.out.println("\tWon: " + game1.playGame());

		final Game game2 = context.getBean("game", Game.class);
		game2.setAwayTeam(context.getBean("zaksa", Team.class));
		System.out.println("\nGame details below:\n");
        System.out.println("\t" + game2);
        System.out.println("\tWon: " + game2.playGame());

        System.out.println("\t game1: " + game1);
        System.out.println("\t game2: " + game2);


		System.out.println();
		System.out.println();
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Destroy method");
    }

}
