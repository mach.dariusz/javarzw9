package com.sda.spring.game.model;

public interface Game {

    void setHomeTeam(Team homeTeam);

    void setAwayTeam(Team homeTeam);

    Team getHomeTeam();

    Team getAwayTeam();

    String playGame();

}
