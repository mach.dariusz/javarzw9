package com.sda.spring.game.model;

public interface Team {

    String getTeamName();
    
}
