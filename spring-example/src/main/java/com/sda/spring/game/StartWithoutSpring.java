package com.sda.spring.game;

import com.sda.spring.game.model.*;

public class StartWithoutSpring {

    public static void main(String[] args) {

        Team asseco = new Asseco();
        Team skra = new Skra();

        Game volleyballGame = new VolleyballGame();
        volleyballGame.setHomeTeam(asseco);
        volleyballGame.setAwayTeam(skra);

        System.out.println(volleyballGame.playGame());

    }

}
