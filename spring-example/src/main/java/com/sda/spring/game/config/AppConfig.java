package com.sda.spring.game.config;

import com.sda.spring.game.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

import java.util.List;
import java.util.Set;

@Configuration
@Import(CustomConfig.class)
@ComponentScan("com.sda")
public class AppConfig {

    private final Team awayTeam;
    private final Team homeTeam;

    private List<Team> teams;

    private Set<Team> teamsAsSet;

//    public AppConfig(@Qualifier("asseco") Team awayTeam, @Qualifier("skra") Team homeTeam) {
//        this.awayTeam = awayTeam;
//        this.homeTeam = homeTeam;
//    }

    public AppConfig(List<Team> teams, Set<Team> teamsAsSet) {

        System.out.println("AppConfig construtor: " + teams);
        System.out.println("AppConfig construtor - set: " + teamsAsSet);

        this.teams = teams;
        this.awayTeam = teams.get(0);
        this.homeTeam = teams.get(1);
    }

    @Bean
    @Scope("prototype")
    public Game game() {
        return new VolleyballGame(homeTeam, awayTeam);
    }

}
