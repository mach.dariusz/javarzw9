package com.sda.spring.game.model;

import org.springframework.stereotype.Component;

@Component
public class Zaksa implements Team {

    @Override
    public String getTeamName() {
        return "Zaksa Kędzierzyn Koźle";
    }

    @Override
    public String toString() {
        return getTeamName();
    }
}
