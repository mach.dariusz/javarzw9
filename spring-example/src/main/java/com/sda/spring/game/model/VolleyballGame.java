package com.sda.spring.game.model;

public class VolleyballGame implements Game {

    private Team homeTeam;
    private Team awayTeam;

    public VolleyballGame() {
    }

    public VolleyballGame(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    @Override
    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    @Override
    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    @Override
    public Team getHomeTeam() {
        return homeTeam;
    }

    @Override
    public Team getAwayTeam() {
        return awayTeam;
    }

    @Override
    public String playGame() {
        return Math.random() > 0.5 ? homeTeam.getTeamName() : awayTeam.getTeamName();
    }


    @Override
    public String toString() {
        return String.format("VolleyballGame: %s vs %s", homeTeam, awayTeam);
    }
}
