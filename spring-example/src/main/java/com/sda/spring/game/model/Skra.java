package com.sda.spring.game.model;

import org.springframework.stereotype.Component;

@Component
public class Skra implements Team {

    @Override
    public String getTeamName() {
        return "Skra Bełchatów";
    }

    @Override
    public String toString() {
        return getTeamName();
    }

}
