package com.sda.spring.game.model;

import org.springframework.stereotype.Component;

@Component
public class Asseco implements Team {

    @Override
    public String getTeamName() {
        return "Asseco Resovia";
    }

    @Override
    public String toString() {
        return getTeamName();
    }
}
