package com.sda.pg.backend.repository;

import com.sda.pg.backend.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EmployeeRepository extends MongoRepository<Employee, String> {

    Employee findByPesel(String pesel);

    List<Employee> findByLastName(String lastName);
    List<Employee> findByLastNameLikeIgnoreCase(String lastName);
}
