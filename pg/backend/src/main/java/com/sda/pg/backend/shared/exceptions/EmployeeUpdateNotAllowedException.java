package com.sda.pg.backend.shared.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class EmployeeUpdateNotAllowedException extends RuntimeException {
}

