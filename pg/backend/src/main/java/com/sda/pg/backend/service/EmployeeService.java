package com.sda.pg.backend.service;

import com.sda.pg.backend.model.Employee;
import com.sda.pg.backend.repository.EmployeeRepository;
import com.sda.pg.backend.shared.exceptions.EmployeeUpdateNotAllowedException;
import com.sda.pg.backend.shared.exceptions.PeselIsRequiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Optional<Employee> create(Employee employee) {

        // check if exist
        Employee prevEmployee = employeeRepository.findByPesel(employee.getPesel());
        if (prevEmployee != null) {
            // employee exist
            return Optional.empty();
        } else {
            // if not exist, then return new
            return Optional.of(employeeRepository.save(employee));
        }
    }

    /**
     *
     * @param id
     * @param employee
     * @return Optional<Employee>
     * @throws EmployeeUpdateNotAllowedException if id is not equal to employee.id
     */
    public Optional<Employee> update(String id, Employee employee) {
        // check if id is equal employee.id - we can update only whe above are equals
        if (id.equals(employee.getId())) {

            // check if employee exist in DB
            Optional<Employee> employeeFromDB = employeeRepository.findById(id);
            if (employeeFromDB.isPresent()) {

                // then update
                return Optional.of(employeeRepository.save(employee));

            } else {
                return Optional.empty(); // empty, throw NotFoundException in controller
            }

        } else {
            // throw some exception
            throw new EmployeeUpdateNotAllowedException();
        }
    }

    public Optional<Employee> delete(String id) {

        // checkIfExist
        Optional<Employee> employee = employeeRepository.findById(id);

        // delete
        employeeRepository.deleteById(id);

        return employee;

    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> findById(String id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getEmployeesByName(String name) {
        return employeeRepository.findByLastNameLikeIgnoreCase(name);
    }
}
