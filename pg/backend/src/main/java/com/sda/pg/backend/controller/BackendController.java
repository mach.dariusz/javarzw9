package com.sda.pg.backend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class BackendController {

    @GetMapping("/getApplicationName")
    public ResponseEntity<String> getApplicationName() {
        return new ResponseEntity<>("backend", HttpStatus.OK);
    }

}
