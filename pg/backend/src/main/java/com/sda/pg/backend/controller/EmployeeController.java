package com.sda.pg.backend.controller;

import com.sda.pg.backend.model.Employee;
import com.sda.pg.backend.service.EmployeeService;
import com.sda.pg.backend.shared.exceptions.EmployeeAlreadyExistsException;
import com.sda.pg.backend.shared.exceptions.EmployeeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getEmployees() {
        return ResponseEntity.ok(employeeService.getAll());
    }

    @GetMapping("/employees/search")
    public ResponseEntity<List<Employee>> getEmployeesByName(@RequestParam String name) {
        return new ResponseEntity<List<Employee>>(
                employeeService.getEmployeesByName(name),
                HttpStatus.OK);
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable() String id) {
        return ResponseEntity.ok(
                employeeService.findById(id)
                        .orElseThrow(EmployeeNotFoundException::new) // if not found return 404
        );
    }

    @PostMapping("/employees")
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody Employee employee) {
        return new ResponseEntity<Employee>(
                employeeService.create(employee)
                        .orElseThrow(() -> new EmployeeAlreadyExistsException()),
                HttpStatus.CREATED);
    }

    @PatchMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable() String id, @Valid @RequestBody Employee employee) {
        return ResponseEntity.ok(
                employeeService.update(id, employee)
                        .orElseThrow(() -> new EmployeeNotFoundException())
        );
    }


    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable() String id) {
        return new ResponseEntity<Employee>(
                employeeService.delete(id)
                        .orElseThrow(() -> new EmployeeNotFoundException()),
                HttpStatus.OK);
    }
}
