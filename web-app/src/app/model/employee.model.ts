export interface Employee {
  id?: string;
  firstName: string;
  lastName: string;
  pesel: string;
}
