import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AlertModule } from 'ngx-bootstrap/alert';
import { EmployeesComponent } from './pages/employees/employees.component';

import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateEditEmployeeComponent } from './pages/create-edit-employee/create-edit-employee.component';
import { InnerHeaderComponent } from './shared/components/inner-header/inner-header.component';

import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { environment } from '../environments/environment';
import { AuthState } from './shared/state/auth/auth.state';
import { LoginComponent } from './pages/login/login.component';

import { NavigationHandler } from './shared/handler/navigation.handler';

const states = [AuthState];

const emptyFn = () => {
  return () => { }
};

@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    DashboardComponent,
    CreateEditEmployeeComponent,
    InnerHeaderComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AlertModule.forRoot(),
    FontAwesomeModule,
    ReactiveFormsModule,
    NgxsModule.forRoot(states, { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production })
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: emptyFn,
      deps: [NavigationHandler],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
