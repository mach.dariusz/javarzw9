import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../model/employee.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  private readonly baseUrl = environment.baseUrl;
  private readonly employeeUrl = `${this.baseUrl}/employees`;

  constructor(private http: HttpClient) { }

  getAllEmployees(): Observable<Employee[]> {
    console.log('[EmployeesService] in getAllEmployees()');

    return this.http.get<Employee[]>(this.employeeUrl);
  }

  getEmployeeById(id: string): Observable<Employee> {
    return this.http.get<Employee>(`${this.employeeUrl}/${id}`);
  }

  createEmployee(employee: Employee): Observable<Employee> {
    console.log('[EmployeesService] in createEmployee()', employee);

    return this.http.post<Employee>(this.employeeUrl, employee);
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    console.log('[EmployeesService] in updateEmployee()', employee);

    // check if employee.id exist
    if (employee.id) {

      return this.http.patch<Employee>(`${this.employeeUrl}/${employee.id}`, employee);

    } else {
      // this shouldn't happen
      alert('You cannot update not saved employee ...');
      throw new Error('You cannot update not saved employee ...');
    }

  }

  deleteEmployeeById(id: string): Observable<Employee> {
    console.log(`[EmployeesService] in deleteEmployeeById(${id})`);

    return this.http.delete<Employee>(`${this.employeeUrl}/${id}`);
  }

}
