import { Component, OnInit } from '@angular/core';
import { Employee } from '../../model/employee.model';
import { EmployeesService } from '../../service/employees.service';
import { faUserEdit, faUserPlus, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[];

  icons = {
    create: faUserPlus,
    edit: faUserEdit,
    delete: faTrashAlt,
    details: faInfoCircle,
  };

  constructor(
    private employeesService: EmployeesService,
    private router: Router,
  ) {
    console.log('constructor in EmployeesComponent');
  }

  ngOnInit(): void {
    console.log('ngOnInit in EmployeesComponent');

    this.employeesService.getAllEmployees().subscribe(response => {
      console.log('response: ', response);
      this.employees = response;
    });
  }

  goToCreateEmployee(): void {
    this.router.navigate(['/employees/create']);
  }

  viewDetails(employee: Employee): void {
    this.router.navigate([`/employees/${employee.id}`]);
  }

  editEmployee(employee: Employee): void {
    console.log('clicked on edit employee:', employee.lastName + ' ' + employee.firstName);

    this.router.navigate([`/employees/edit/${employee.id}`]);
  }

  deleteEmployee(employee: Employee): void {
    console.log('clicked on delete employee:', employee.lastName + ' ' + employee.firstName);

    this.employeesService.deleteEmployeeById(employee.id).subscribe(
      response => {
        console.log('Delete employee: ', response);
        this.employees = this.employees.filter(emp => emp.id !== response.id);
      },
      error => console.error('Error in deleteEmployee():', error),
    );
  }

}
