import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Employee } from '../../model/employee.model';
import { EmployeesService } from '../../service/employees.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

enum OperationEnum {
  CREATE = 'CREATE',
  EDIT = 'EDIT',
  READ = 'READ',
}

@Component({
  selector: 'app-create-edit-employee',
  templateUrl: './create-edit-employee.component.html',
  styleUrls: ['./create-edit-employee.component.scss']
})
export class CreateEditEmployeeComponent implements OnInit {

  employeeForm: FormGroup;

  mode: OperationEnum;

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeesService,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    console.log('[CreateEditEmployeeComponent] ngOnInit()');

    // set CREATE / EDIT / READ
    this.setMode();

    this.prepareForm();
  }

  setMode(): void {
    const paramId = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('Route params:', paramId);

    if (paramId) {
      // edit || read
      if (this.router.url.startsWith('/employees/edit')) {
        // edit
        this.mode = OperationEnum.EDIT;
      } else {
        // read
        this.mode = OperationEnum.READ;
      }
    } else {
      // create
      this.mode = OperationEnum.CREATE;
    }
  }

  prepareForm(): void {
    this.employeeForm = this.fb.group({
      id: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      pesel: ['', [Validators.required, Validators.pattern(/[0-9]+/), Validators.minLength(11), Validators.maxLength(11)]],
      // address: this.fb.group({
      //   street: [''],
      //   city: [''],
      //   state: [''],
      //   zip: ['']
      // }),
    });

    if (this.mode !== OperationEnum.CREATE) {
      // fetch employee object by id
      const empId = this.activatedRoute.snapshot.paramMap.get('id');
      this.fetchEmployee(empId);
    }
  }

  fetchEmployee(id: string): void {
    this.employeeService.getEmployeeById(id).subscribe(response => {
      // success
      this.employeeForm.setValue(response);
      this.prepareView();
    }, error => {
      // failure
      alert('Cannot load employee\n' + JSON.stringify(error));
    },
    );
  }

  prepareView(): void {
    if (this.mode === OperationEnum.READ) {
      this.employeeForm.disable();
    }
  }
  isValid(controlName: string): boolean {
    return this.employeeForm.controls[controlName].errors
      && (this.employeeForm.controls[controlName].dirty
        || this.employeeForm.controls[controlName].touched);
  }

  save(employee: Employee): void {
    if (this.mode === OperationEnum.CREATE) {
      this.create(employee);
    } else {
      this.update(employee);
    }
  }

  create(employee: Employee): void {
    console.log('[CreateEditEmployeeComponent] Try to create employee: ', employee);

    this.employeeService.createEmployee(employee).subscribe(
      response => {
        console.log('[CreateEditEmployeeComponent] Employee created employee:', response);

        this.location.back();
      },
      error => {
        console.error('[CreateEditEmployeeComponent] createEmployee() error', error);

        alert('[CreateEditEmployeeComponent] createEmployee() error: \n' + JSON.stringify(error));
      },
    );
  }

  update(employee: Employee): void {
    console.log('[CreateEditEmployeeComponent] Try to update employee: ', employee);

    this.employeeService.updateEmployee(employee).subscribe(
      response => {
        // success
        this.location.back();
      },
      error => {
        // erorr
        console.error('[CreateEditEmployeeComponent] updateEmployee() error', error);

        alert('[CreateEditEmployeeComponent] updateEmployee() error: \n' + JSON.stringify(error));
      }
    );
  }


}
