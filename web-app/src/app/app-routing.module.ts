import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EmployeesComponent } from './pages/employees/employees.component';
import { CreateEditEmployeeComponent } from './pages/create-edit-employee/create-edit-employee.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginGuard } from './shared/guards/login.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [LoginGuard] },
  { path: 'employees/create', component: CreateEditEmployeeComponent, canActivate: [LoginGuard] },      // CREATE
  { path: 'employees/edit/:id', component: CreateEditEmployeeComponent, canActivate: [LoginGuard] },    // EDIT
  { path: 'employees/:id', component: CreateEditEmployeeComponent, canActivate: [LoginGuard] },         // READ
  { path: 'employees', component: EmployeesComponent, canActivate: [LoginGuard] },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
