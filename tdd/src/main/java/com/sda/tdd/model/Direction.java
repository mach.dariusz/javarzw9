package com.sda.tdd.model;

import java.util.concurrent.TimeUnit;

/**
 * operation could be int -1, 0, 1:
 * if -1 then remove step
 * if 0 then do nothing
 * if 1 then add step
 */
public enum Direction {

    RIGHT(0, 1),
    LEFT(0, -1),
    TOP(-1, 0),
    BOTTOM(1, 0),
    RIGHT_TOP(-1, 1),
    LEFT_TOP(-1, -1),
    RIGHT_BOTTOM(1, 1),
    LEFT_BOTTOM(1, -1);

    private int operationX;
    private int operationY;

    Direction(int operationX, int operationY) {
        this.operationX = operationX;
        this.operationY = operationY;
    }

    public Coordinates getNextCoordinates(Coordinates startingFrom, int searchingStep) {
        return new Coordinates(getX(startingFrom, searchingStep), getY(startingFrom, searchingStep));
    }

    public boolean shouldSearchInDirection(String word, Coordinates startingFrom, char[][] lettersTable) {
        if (startingFrom == new Coordinates(1, 4)) {
            System.out.println("debug");
        }

        boolean canSearch = canSearchOnXAxis(word, startingFrom, lettersTable) && canSearchOnYAxis(word, startingFrom, lettersTable);

        return canSearch;
    }

    private boolean canSearchOnXAxis(String word, Coordinates startingFrom, char[][] lettersTable) {
        switch (operationX) {
            case 0:
                return true;
            case -1:
                return startingFrom.getX() - (word.length() - 1) >= 0;
            case 1:
                return startingFrom.getX() + (word.length() - 1) < lettersTable.length;
        }

        throw new IllegalArgumentException();
    }

    private boolean canSearchOnYAxis(String word, Coordinates startingFrom, char[][] lettersTable) {
        switch (operationY) {
            case 0:
                return true;
            case -1:
                return startingFrom.getY() - (word.length() - 1) >= 0;
            case 1:
                return startingFrom.getY() + (word.length() - 1) < lettersTable[startingFrom.getX()].length;
        }

        throw new IllegalArgumentException();
    }

    private int getX(Coordinates startingFrom, int searchingStep) {
        switch (operationX) {
            case 0:
                return startingFrom.getX();
            case -1:
                return startingFrom.getX() - searchingStep;
            case 1:
                return startingFrom.getX() + searchingStep;
        }

        throw new IllegalArgumentException();
    }

    private int getY(Coordinates startingFrom, int searchingStep) {
        switch (operationY) {
            case 0:
                return startingFrom.getY();
            case -1:
                return startingFrom.getY() - searchingStep;
            case 1:
                return startingFrom.getY() + searchingStep;
        }

        throw new IllegalArgumentException();
    }

}
