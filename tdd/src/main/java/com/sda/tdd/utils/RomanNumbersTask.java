package com.sda.tdd.utils;

import com.sda.tdd.exceptions.MoreThanOneRomanLiteralException;
import com.sda.tdd.exceptions.RomanNumberNotValidException;
import com.sda.tdd.exceptions.ZeroOrEmptyRomanLiteralException;
import com.sda.tdd.model.RomanLiteral;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RomanNumbersTask {

    /**
     * @param romanNumber
     * @return IllegalArgumentException if RomanLiteral not exists, ZeroOrEmptyRomanLiteralException if null or empty string passed to the method, MoreThanOneRomanLiteralException if string length passed to method is greater than 1
     */
    public static int returnIntegerFromRomanSingleLiteral(String romanNumber) {

        if (romanNumber == null || romanNumber.isEmpty()) {
            throw new ZeroOrEmptyRomanLiteralException();
        }

        if (romanNumber.length() > 1) {
            throw new MoreThanOneRomanLiteralException();
        }

        return RomanLiteral.valueOf(romanNumber).getIntegerValue();
    }

    public static int returnIntegerFromRomanSingleLiteral(RomanLiteral romanLiteral) {
        return romanLiteral.getIntegerValue();
    }

    public static int calcIntegerFromRomanWord(String romanLiteralWord) {

        // check if romanLiteralWord is valid
        if (!isRomanLiteralWordValid(romanLiteralWord)) {
            throw new RomanNumberNotValidException();
        }
        ;

        // change string to Character[]
        Character[] letters = romanLiteralWord.chars().mapToObj(item -> (char) item).toArray(Character[]::new);
        // change Character[] to List<Integer>
        List<Integer> numbers = Arrays.stream(letters).map(letter -> returnIntegerFromRomanSingleLiteral(letter.toString())).collect(Collectors.toList());

        // prepare array numbers for adding
        for (int i = 0; i < numbers.size(); i++) {
            if (i != numbers.size() - 1) { // check if we have next element
                if (numbers.get(i) < numbers.get(i + 1)) {  // check if current number is lower than next number
                    numbers.set(i, numbers.get(i + 1) - numbers.get(i)); // subtract next and current
                    numbers.remove(i + 1); // remove next
                }
            }
        }

        Integer value = numbers.stream().reduce(0, (a, b) -> a + b);

        return value;
    }

    /**
     * dummy implementation
     *
     * @param romanLiteralWord
     * @return
     */
    private static boolean isRomanLiteralWordValid(String romanLiteralWord) {

        // VV, LL, DD - forbidden
        String[] arrVV = romanLiteralWord.split("VV");
        String[] arrLL = romanLiteralWord.split("LL");
        String[] arrDD = romanLiteralWord.split("DD");

        if (arrVV.length > 1 || arrLL.length > 1 || arrDD.length > 1) {
            return false;
        }

        // change string to Character[]
        Character[] letters = romanLiteralWord.chars().mapToObj(item -> (char) item).toArray(Character[]::new);
        // change Character[] to List<Integer>
        List<Integer> numbers = Arrays.stream(letters).map(letter -> returnIntegerFromRomanSingleLiteral(letter.toString())).collect(Collectors.toList());

        // I X C - can be before larger number (1, 10, 100)
        for (int i = 0; i < numbers.size(); i++) {
            if (i != numbers.size() - 1) {
                if (numbers.get(i) < numbers.get(i + 1)) {
                    if (!(numbers.get(i) == 1 || numbers.get(i) == 10 || numbers.get(i) == 100)) {
                        return false;
                    }
                }
            }
        }

        int max = numbers.stream().max(Integer::compareTo).get();

        if (numbers.indexOf(max) > 1) {
            return false;
        }


        return true;
    }
}


