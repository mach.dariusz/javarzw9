package com.sda.tdd.utils;

public class FizzBuzzTask {

    /**
     * @param number
     * @return 'Fizz' if number divisible by 3; 'Buzz' if number divisible by 5; 'FizzBuzz' if number divisible by 3 and 5
     */
    public static String returnResult(int number) {
        if (number != 0) {
            if (number % 5 == 0 && number % 3 == 0) {
                return "FizzBuzz";
            }

            if (number % 3 == 0) {
                return "Fizz";
            }

            if (number % 5 == 0) {
                return "Buzz";
            }
        }


        return String.valueOf(number);
    }
}
