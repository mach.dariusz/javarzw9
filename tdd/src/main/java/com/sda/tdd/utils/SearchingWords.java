package com.sda.tdd.utils;

import com.sda.tdd.model.Coordinates;
import com.sda.tdd.model.Direction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchingWords {

    public static boolean checkIfLetterExistAt(char letter, Coordinates coordinates, char[][] lettersTable) {
        return lettersTable[coordinates.getX()][coordinates.getY()] == letter;
    }

    public static List<Coordinates> findAllOccurrencesOfLetter(char letter, char[][] lettersTable) {

        List<Coordinates> occurrencesOfLetter = new ArrayList<>();

        for (int row = 0; row < lettersTable.length; row++) {

            for (int col = 0; col < lettersTable[row].length; col++) {
                // System.out.printf("Letter in table at [%s,%s]: %s\n", row, col, lettersTable[row][col]);

                Coordinates currentCoordinates = new Coordinates(row, col);
                if (checkIfLetterExistAt(letter, currentCoordinates, lettersTable)) {
                    occurrencesOfLetter.add(currentCoordinates);
                }

            }

        }

        return occurrencesOfLetter;
    }

    /**
     * @param word
     * @param startingFrom
     * @param direction
     * @param lettersTable
     * @return List<Coordinates> if word exists in direction or null otherwise
     */
    public static List<Coordinates> findWordOccurrenceStartingFromInDirection(String word, Coordinates startingFrom, Direction direction, char[][] lettersTable) {
        char[] letters = word.toCharArray();
        List<Coordinates> result = new ArrayList<>();

        // add 1st letter to result ArrayList as it was already found
        result.add(startingFrom);

        // System.out.printf("Letter %s found at [%s,%s]\n", letters[0], startingFrom.getX(), startingFrom.getY());

        for (int i = 1; i < word.length(); i++) { // start i from 1, because 1st letter position is known
            Coordinates nextCoordinates = direction.getNextCoordinates(startingFrom, i);
            if (checkIfLetterExistAt(letters[i], nextCoordinates, lettersTable)) {
                // System.out.printf("Letter %s found at [%s,%s]\n", letters[i], nextCoordinates.getX(), nextCoordinates.getY());

                result.add(nextCoordinates);
            } else {
                result = null;
                break;
            }
        }

        return result;
    }

    public static List<List<Coordinates>> findWordAllOccurrences(String word, char[][] lettersTable) {
        List<List<Coordinates>> result = new ArrayList<>();

        // step 1
        char firstLetter = word.charAt(0);
        List<Coordinates> firstLetterOccurrences = findAllOccurrencesOfLetter(firstLetter, lettersTable);

        // step 2
        for (Coordinates firstLetterOccurrencesCoordinates : firstLetterOccurrences) {

            // step 3
            Arrays.stream(Direction.values()).forEach(direction -> {

                if (!direction.shouldSearchInDirection(word, firstLetterOccurrencesCoordinates, lettersTable)) {
                    System.out.printf("\nWe cannot search for %s in direction %s starting at %s\n", word, direction, firstLetterOccurrencesCoordinates);
                    return;
                }

                System.out.printf("\nStarting searching '%s' in direction %s starting at %s\n", word, direction, firstLetterOccurrencesCoordinates);

                // step 4
                List<Coordinates> wordOccurrenceStartingFromInDirection = findWordOccurrenceStartingFromInDirection(word, firstLetterOccurrencesCoordinates, direction, lettersTable);

                // step 5
                if (wordOccurrenceStartingFromInDirection != null) {
                    System.out.printf("\tFound word '%s' starting at %s in direction %s\n", word, firstLetterOccurrencesCoordinates, direction);
                    System.out.println("\t\t" + wordOccurrenceStartingFromInDirection);
                    result.add(wordOccurrenceStartingFromInDirection);
                }

            });

        }

        return result;
    }
}
