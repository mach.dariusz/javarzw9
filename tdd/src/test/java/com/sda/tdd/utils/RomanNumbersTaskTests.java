package com.sda.tdd.utils;

import com.sda.tdd.exceptions.MoreThanOneRomanLiteralException;
import com.sda.tdd.exceptions.RomanNumberNotValidException;
import com.sda.tdd.exceptions.ZeroOrEmptyRomanLiteralException;
import com.sda.tdd.model.RomanLiteral;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RomanNumbersTaskTests {

    @Test
    void testRomanLiteralEnum() {
        Assertions.assertEquals(RomanLiteral.valueOf("X"), RomanLiteral.X);
        Assertions.assertEquals(RomanLiteral.valueOf("C"), RomanLiteral.C);
    }

    @DisplayName("Roman numbers should be correct integers")
    @Test
    void romanTestCase1() {
        Assertions.assertEquals(1, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("I"));
        Assertions.assertEquals(5, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("V"));
        Assertions.assertEquals(10, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("X"));
        Assertions.assertEquals(50, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("L"));
        Assertions.assertEquals(100, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("C"));
        Assertions.assertEquals(500, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("D"));
        Assertions.assertEquals(1000, RomanNumbersTask.returnIntegerFromRomanSingleLiteral("M"));

        Assertions.assertEquals(500, RomanNumbersTask.returnIntegerFromRomanSingleLiteral(RomanLiteral.D));
        Assertions.assertEquals(1000, RomanNumbersTask.returnIntegerFromRomanSingleLiteral(RomanLiteral.M));
        Assertions.assertEquals(10, RomanNumbersTask.returnIntegerFromRomanSingleLiteral(RomanLiteral.X));

    }

    @DisplayName("Expected exception when pass more than one literal to method returnIntegerFromRomanSingleLiteral")
    @Test
    void romanTestCas2() {
        Assertions.assertThrows(ZeroOrEmptyRomanLiteralException.class, () -> {
            String literal = null;
            RomanNumbersTask.returnIntegerFromRomanSingleLiteral(literal); // null
            RomanNumbersTask.returnIntegerFromRomanSingleLiteral(""); // empty
        });

        Assertions.assertThrows(MoreThanOneRomanLiteralException.class, () -> {
            RomanNumbersTask.returnIntegerFromRomanSingleLiteral("XX"); // more than one literal
            RomanNumbersTask.returnIntegerFromRomanSingleLiteral("III"); // more than one literal
        });

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            RomanNumbersTask.returnIntegerFromRomanSingleLiteral("T"); // not exists
        });
    }

    @Test
    void calcIntegerNumberFromRomanLiteralWord() {

        Assertions.assertEquals(3, RomanNumbersTask.calcIntegerFromRomanWord("III"));
        Assertions.assertEquals(30, RomanNumbersTask.calcIntegerFromRomanWord("XXX"));
        Assertions.assertEquals(2004, RomanNumbersTask.calcIntegerFromRomanWord("MMIV"));
        Assertions.assertEquals(999, RomanNumbersTask.calcIntegerFromRomanWord("CMXCIX"));

    }

    @Test()
    void checkIfRomanWordIsValid() {
        Assertions.assertThrows(RomanNumberNotValidException.class, () -> {
            RomanNumbersTask.calcIntegerFromRomanWord("XXL");
            RomanNumbersTask.calcIntegerFromRomanWord("DM");
            RomanNumbersTask.calcIntegerFromRomanWord("DDM");
            RomanNumbersTask.calcIntegerFromRomanWord("MDD");
        });
        Assertions.assertEquals(3, RomanNumbersTask.calcIntegerFromRomanWord("III"));


    }

}
