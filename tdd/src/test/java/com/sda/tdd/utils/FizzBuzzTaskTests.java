package com.sda.tdd.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FizzBuzzTaskTests {

    @DisplayName("Check if 'Fizz' word will be returned for number divisible by 3")
    @Test
    void checkIfFizz() {
        Assertions.assertEquals("Fizz", FizzBuzzTask.returnResult(3));
        Assertions.assertEquals("Fizz", FizzBuzzTask.returnResult(6));
        Assertions.assertEquals("Fizz", FizzBuzzTask.returnResult(9));
        Assertions.assertEquals("Fizz", FizzBuzzTask.returnResult(12));
    }

    @DisplayName("Check if integer word will be returned for number not divisible by 3")
    @Test
    void checkIfNumber() {
        Assertions.assertEquals("0", FizzBuzzTask.returnResult(0));
        Assertions.assertEquals("1", FizzBuzzTask.returnResult(1));
        Assertions.assertEquals("2", FizzBuzzTask.returnResult(2));
        Assertions.assertEquals("4", FizzBuzzTask.returnResult(4));
        Assertions.assertEquals("7", FizzBuzzTask.returnResult(7));
        Assertions.assertEquals("8", FizzBuzzTask.returnResult(8));
    }

    @DisplayName("Check if 'Buzz' word will be returned for number divisible by 5")
    @Test
    void checkIfBuzz() {
        Assertions.assertEquals("Buzz", FizzBuzzTask.returnResult(5));
        Assertions.assertEquals("Buzz", FizzBuzzTask.returnResult(10));
        Assertions.assertEquals("Buzz", FizzBuzzTask.returnResult(20));
    }

    @DisplayName("Check if 'FizzBuzz' word will be returned for number divisible by 3 && 5")
    @Test
    void checkIfFizzBuzz() {
        Assertions.assertEquals("FizzBuzz", FizzBuzzTask.returnResult(15));
        Assertions.assertEquals("FizzBuzz", FizzBuzzTask.returnResult(30));
        Assertions.assertEquals("FizzBuzz", FizzBuzzTask.returnResult(45));
    }

}
