package com.sda.tdd.utils;

import com.sda.tdd.model.Coordinates;
import com.sda.tdd.model.Direction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class SearchingWordsTests {

    private final char[][] lettersTable = {
            {'A', 'B', 'C', 'K', 'D', 'E'}, // row 0
            {'L', 'T', 'O', 'K', 'A', 'A'}, // row 1
            {'A', 'T', 'O', 'K', 'L', 'B'}, // row 2
            {'L', 'B', 'C', 'A', 'A', 'T'}, // row 3
            {'A', 'C', 'C', 'B', 'L', 'O'}, // row 4
            {'B', 'A', 'B', 'C', 'B', 'A'}, // row 5
    };


    @Test
    void testCheckingIfLetterExistAtPosition() {
        Assertions.assertTrue(SearchingWords.checkIfLetterExistAt('K', new Coordinates(2, 3), lettersTable));
        Assertions.assertTrue(SearchingWords.checkIfLetterExistAt('O', new Coordinates(2, 2), lettersTable));
    }

    @Test
    void testCheckingAllOccurrencesOfLetter() {
        // for letter 'K'
        List<Coordinates> firstLetterOccurrence = new ArrayList<>();
        firstLetterOccurrence.add(new Coordinates(0, 3));
        firstLetterOccurrence.add(new Coordinates(1, 3));
        firstLetterOccurrence.add(new Coordinates(2, 3));

        Assertions.assertEquals(firstLetterOccurrence, SearchingWords.findAllOccurrencesOfLetter('K', lettersTable));
    }

    @Test
    void testSearchingWholeWordInOneDirection() {
        final String word = "KOT";
        final Direction direction = Direction.LEFT;
        final Coordinates firstLetterOccurrence = new Coordinates(1, 3);

        List<Coordinates> wordOccurrence = new ArrayList<>();
        wordOccurrence.add(firstLetterOccurrence);
        wordOccurrence.add(new Coordinates(1, 2));
        wordOccurrence.add(new Coordinates(1, 1));

        Assertions.assertEquals(wordOccurrence, SearchingWords.findWordOccurrenceStartingFromInDirection(word, firstLetterOccurrence, direction, lettersTable));

        // second test
        final Coordinates firstLetterOccurrence2 = new Coordinates(2, 3);

        List<Coordinates> wordOccurrence2 = new ArrayList<>();
        wordOccurrence2.add(firstLetterOccurrence2);
        wordOccurrence2.add(new Coordinates(2, 2));
        wordOccurrence2.add(new Coordinates(2, 1));

        Assertions.assertEquals(wordOccurrence2, SearchingWords.findWordOccurrenceStartingFromInDirection(word, firstLetterOccurrence2, direction, lettersTable));


        // third test
        final String word2 = "ABC";
        final Direction direction2 = Direction.RIGHT_BOTTOM;
        final Coordinates firstLetterOccurrence3 = new Coordinates(2, 0);

        List<Coordinates> wordOccurrence3 = new ArrayList<>();
        wordOccurrence3.add(firstLetterOccurrence3);
        wordOccurrence3.add(new Coordinates(3, 1));
        wordOccurrence3.add(new Coordinates(4, 2));

        Assertions.assertEquals(wordOccurrence3, SearchingWords.findWordOccurrenceStartingFromInDirection(word2, firstLetterOccurrence3, direction2, lettersTable));

    }

    @Test
    void searchAllOccurrencesOfWordInDirectionTest() {
        final String word = "DALAL";

        List<List<Coordinates>> expectedResult = new ArrayList<>();
        List<Coordinates> expectedCoords = new ArrayList<>();
        expectedCoords.add(new Coordinates(0, 4));
        expectedCoords.add(new Coordinates(1, 4));
        expectedCoords.add(new Coordinates(2, 4));
        expectedCoords.add(new Coordinates(3, 4));
        expectedCoords.add(new Coordinates(4, 4));

        expectedResult.add(expectedCoords);

        Assertions.assertEquals(expectedResult, SearchingWords.findWordAllOccurrences(word, lettersTable));
    }
}
