package com.sda.javarzw9.restaurant.model;

import lombok.Data;

@Data
public class Address {

    private String street;
    private String city;
    private String postalCode;

}
