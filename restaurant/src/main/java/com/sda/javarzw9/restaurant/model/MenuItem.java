package com.sda.javarzw9.restaurant.model;

import lombok.Data;

import java.util.List;

@Data
public class MenuItem {

    private String id;
    private String name;
    private List<MenuItemCategory> category;
    private Price price;
    private List<String> ingredients;
}
