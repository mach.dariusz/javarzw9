package com.sda.javarzw9.restaurant.model;

import lombok.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Menu {

    private List<MenuItem> items;

    public void setItems(List<MenuItem> items) {
        AtomicInteger counter = new AtomicInteger(0);
        this.items = items.stream().map(item -> {
            item.setId(String.valueOf(counter.incrementAndGet()));

            return item;
        }).collect(Collectors.toList());
    }
}
