package com.sda.javarzw9.restaurant.config;

import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverter;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
                .authorizeExchange(exchanges -> exchanges
                        .pathMatchers("/restaurants/**").hasRole("app-admin")
                        .anyExchange().authenticated()
                )
                .csrf().disable()
                .oauth2ResourceServer(oauth2 -> oauth2
                        .jwt(jwtSpec -> jwtSpec.jwtAuthenticationConverter(jwtAuthenticationConverter()))
                );

        return http.build();
    }

    private Converter<Jwt, ? extends Mono<? extends AbstractAuthenticationToken>> jwtAuthenticationConverter() {

        ReactiveJwtAuthenticationConverter jwtAuthenticationConverter = new ReactiveJwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new KeycloakCustomJwtConverter());

        return jwtAuthenticationConverter;
    }

    private class KeycloakCustomJwtConverter implements Converter<Jwt, Flux<GrantedAuthority>> {

        @Override
        public Flux<GrantedAuthority> convert(Jwt jwt) {

            List<String> roles = ((Map<String, List<String>>) jwt.getClaims().get("realm_access")).get("roles");
            List<String> rolesList = roles.stream()
                    .map(role -> String.format("ROLE_%s", role))
                    .collect(Collectors.toList());

            String scope = (String) jwt.getClaims().get("scope");
            List<String> scopeList = Arrays.asList(scope.split("\\u0020")).stream()
                    .map(item -> String.format("SCOPE_%s", item))
                    .collect(Collectors.toList());

            List<String> rolesAndScopeList = Stream.concat(rolesList.stream(), scopeList.stream())
                    .collect(Collectors.toList());

            List<SimpleGrantedAuthority> rolesAndScopeAsGrantedAuthorities = rolesAndScopeList.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());

            return Flux.fromIterable(rolesAndScopeAsGrantedAuthorities);
        }

    }
}
