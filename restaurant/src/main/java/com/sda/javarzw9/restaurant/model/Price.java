package com.sda.javarzw9.restaurant.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Price {

    private BigDecimal value;
    private String currency;

}
