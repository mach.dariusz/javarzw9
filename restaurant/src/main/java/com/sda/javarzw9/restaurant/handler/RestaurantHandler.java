package com.sda.javarzw9.restaurant.handler;

import com.sda.javarzw9.restaurant.model.MenuItem;
import com.sda.javarzw9.restaurant.model.Restaurant;
import com.sda.javarzw9.restaurant.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.ServerResponse.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class RestaurantHandler {

    Mono<SecurityContext> context = ReactiveSecurityContextHolder.getContext();

    private final RestaurantRepository repository;

    public Mono<ServerResponse> getAllRestaurants(ServerRequest request) {
        return repository.findAll()
                .collectList()
                .flatMap(restaurants -> ok().contentType(APPLICATION_JSON).bodyValue(restaurants));
    }

    public Mono<ServerResponse> createRestaurant(ServerRequest request) {

        return request.bodyToMono(Restaurant.class)
                .flatMap(restaurant -> repository
                        .findByName(restaurant.getName())
                        .switchIfEmpty(repository.save(restaurant)))
                .flatMap(restaurant -> created(URI.create(String.format("/restaurants/%s", restaurant.getId())))
                        .contentType(APPLICATION_JSON)
                        .bodyValue(restaurant)
                ).switchIfEmpty(ServerResponse.badRequest().build());
    }

    public Mono<ServerResponse> getRestaurantById(ServerRequest request) {
        String restaurantId = request.pathVariable("id");

        return repository.findById(restaurantId)
                .flatMap(restaurant -> ok().contentType(APPLICATION_JSON).bodyValue(restaurant))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> deleteRestaurantById(ServerRequest request) {
        String restaurantId = request.pathVariable("id");

        return repository.findById(restaurantId)
                .flatMap(restaurant -> {
                    repository.deleteById(restaurantId)
                            .doOnSuccess(aVoid -> log.info(String.format("Restaurant with id: %s deleted", restaurant.getId())))
                            .subscribe();

                    return Mono.just(restaurant);
                })
                .flatMap(restaurant -> ok().contentType(APPLICATION_JSON).bodyValue(restaurant))
                .switchIfEmpty(ServerResponse.badRequest().build());
    }

    public Mono<ServerResponse> updateRestaurantById(ServerRequest request) {
        String restaurantId = request.pathVariable("id");

        return repository.findById(restaurantId)
                .flatMap(restaurant -> request.bodyToMono(Restaurant.class))
                .flatMap(restaurant -> {
                    if (restaurant.getId() != null) {
                        return repository.save(restaurant);
                    } else {
                        return Mono.empty();
                    }
                })
                .flatMap(restaurant -> accepted().contentType(APPLICATION_JSON).bodyValue(restaurant))
                .switchIfEmpty(ServerResponse.badRequest().build());
    }

    public Mono<ServerResponse> getRestaurantMenu(ServerRequest request) {
        String restaurantId = request.pathVariable("id");

        return repository.findById(restaurantId)
                .map(restaurant -> restaurant.getMenu())
                .flatMap(restaurantMenu -> ok().contentType(APPLICATION_JSON).bodyValue(restaurantMenu))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> getRestaurantMenuItem(ServerRequest request) {
        String restaurantId = request.pathVariable("id");
        String menuItemId = request.pathVariable("itemId");

        return repository.findById(restaurantId)
                .map(restaurant -> restaurant.getMenu())
                .flatMap(restaurantMenu -> {
                    Optional<MenuItem> menuItemOptional = restaurantMenu.getItems().stream()
                            .filter(item -> item.getId().equals(menuItemId)).findAny();

                    return menuItemOptional.isPresent() ? Mono.just(menuItemOptional.get()) : Mono.empty();
                })
                .flatMap(menuItem -> ok().contentType(APPLICATION_JSON).bodyValue(menuItem))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> getPrincipalDetails(ServerRequest request) {

        return context.flatMap(securityContext -> Mono.just(securityContext.getAuthentication()))
                .flatMap(authentication -> ok().contentType(APPLICATION_JSON).bodyValue(authentication));
    }
}
