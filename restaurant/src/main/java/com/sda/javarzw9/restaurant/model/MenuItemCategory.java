package com.sda.javarzw9.restaurant.model;


public enum MenuItemCategory {
    STARTERS,
    SIDES,
    MAIN,
    DRINKS;
}
