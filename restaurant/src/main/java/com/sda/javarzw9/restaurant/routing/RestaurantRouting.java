package com.sda.javarzw9.restaurant.routing;

import com.sda.javarzw9.restaurant.handler.RestaurantHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@RequiredArgsConstructor
public class RestaurantRouting {

    private final RestaurantHandler handler;

    @Bean
    public RouterFunction<ServerResponse> restaurantRoutes() {

        return route()
                .path("/restaurants", builder -> builder
                        .GET("", handler::getAllRestaurants)
                        .GET("/{id}", handler::getRestaurantById)
                        .POST("", handler::createRestaurant)
                        .PUT("/{id}", handler::updateRestaurantById)
                        .DELETE("/{id}", handler::deleteRestaurantById)

                        .GET("/{id}/menu", handler::getRestaurantMenu)
                        .GET("/{id}/menu/{itemId}", handler::getRestaurantMenuItem)
                )
                .path("/test", builder -> builder
                        .GET("", handler::getPrincipalDetails)
                ).build();
    }

}
