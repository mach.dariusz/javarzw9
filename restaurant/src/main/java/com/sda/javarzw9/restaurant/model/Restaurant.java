package com.sda.javarzw9.restaurant.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Document("restaurants")
@Data
public class Restaurant {

    @Id
    private String id;

    @NotBlank
    private String name;
    private Address address;
    private Menu menu;

}
