package com.sda.reactive.config;

import com.sda.reactive.model.Employee;
import com.sda.reactive.repository.EmployeeRepository;
import com.sda.reactive.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class RouterExample {

    @Autowired
    private EmployeeService service;
    @Autowired
    private EmployeeRepository repository;

    @Bean
    RouterFunction<ServerResponse> routes() {

        return route(GET("/emps"), request -> {
            return ok().body(service.getAllEmployees(), Employee.class);

        }).and(route(POST("/emps"), request -> {

            return ok().body(
                    request.bodyToMono(Employee.class)
                            .doOnSuccess(employee -> System.out.println("Success in getting body object"))
                            .flatMap(employee -> {
                                return repository.findByEmail(employee.getEmail()).switchIfEmpty(repository.save(employee));
                            }),
                    Employee.class
            );

        }));
    }
}


