package com.sda.reactive.repository;

import com.sda.reactive.model.Employee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface EmployeeRepository extends ReactiveMongoRepository<Employee, String> {

    Mono<Employee> findByEmail(String email);

}
