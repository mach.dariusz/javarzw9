package com.sda.reactive.service;

import com.sda.reactive.model.Employee;
import com.sda.reactive.repository.EmployeeRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Flux<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Mono<Employee> findById(String id) {
        return employeeRepository.findById(id);
    }

    public Mono<Employee> findByEmail(String email) {
        return employeeRepository.findByEmail(email);
    }

    public Mono<Employee> createEmployee(Employee employee) {

        return employeeRepository.findByEmail(employee.getEmail())
                .switchIfEmpty(employeeRepository.save(employee));

        // alternative to above
        /*
                .flatMap(emp -> {
                    if (emp != null) {
                        return Mono.just(emp);
                    } else {
                        return employeeRepository.save(employee);
                    }
                });
         */
    }

    public Mono<Employee> updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

}
